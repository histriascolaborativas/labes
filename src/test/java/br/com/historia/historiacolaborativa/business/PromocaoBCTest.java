

package br.com.historia.historiacolaborativa.business;

import static org.junit.Assert.*;
import java.util.*;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import br.gov.frameworkdemoiselle.junit.DemoiselleRunner;
import br.com.historia.historiacolaborativa.domain.Promocao;
import br.com.historia.historiacolaborativa.business.PromocaoBC;

@RunWith(DemoiselleRunner.class)
public class PromocaoBCTest {

    @Inject
	private PromocaoBC promocaoBC;
	
	@Before
	public void before() {
		for (Promocao promocao : promocaoBC.findAll()) {
			promocaoBC.delete(promocao.getId_promocao());
		}
	}	
	
	
	@Test
	public void testInsert() {
				
		// modifique para inserir dados conforme o construtor
		Promocao promocao = new Promocao("descricao");
		promocaoBC.insert(promocao);
		List<Promocao> listOfPromocao = promocaoBC.findAll();
		assertNotNull(listOfPromocao);
		assertEquals(1, listOfPromocao.size());
	}	
	
	@Test
	public void testDelete() {
		
		// modifique para inserir dados conforme o construtor
		Promocao promocao = new Promocao("descricao");
		promocaoBC.insert(promocao);
		
		List<Promocao> listOfPromocao = promocaoBC.findAll();
		assertNotNull(listOfPromocao);
		assertEquals(1, listOfPromocao.size());
		
		promocaoBC.delete(promocao.getId_promocao());
		listOfPromocao = promocaoBC.findAll();
		assertEquals(0, listOfPromocao.size());
	}
	
	@Test
	public void testUpdate() {
		// modifique para inserir dados conforme o construtor
		Promocao promocao = new Promocao("descricao");
		promocaoBC.insert(promocao);
		
		List<Promocao> listOfPromocao = promocaoBC.findAll();
		Promocao promocao2 = (Promocao)listOfPromocao.get(0);
		assertNotNull(listOfPromocao);

		// alterar para tratar uma propriedade existente na Entidade Promocao
		// promocao2.setUmaPropriedade("novo valor");
		promocaoBC.update(promocao2);
		
		listOfPromocao = promocaoBC.findAll();
		Promocao promocao3 = (Promocao)listOfPromocao.get(0);
		
		// alterar para tratar uma propriedade existente na Entidade Promocao
		// assertEquals("novo valor", promocao3.getUmaPropriedade());
	}

}