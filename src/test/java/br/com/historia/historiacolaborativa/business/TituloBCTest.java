

package br.com.historia.historiacolaborativa.business;

import static org.junit.Assert.*;
import java.util.*;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import br.gov.frameworkdemoiselle.junit.DemoiselleRunner;
import br.com.historia.historiacolaborativa.domain.Titulo;
import br.com.historia.historiacolaborativa.business.TituloBC;

@RunWith(DemoiselleRunner.class)
public class TituloBCTest {

    @Inject
	private TituloBC tituloBC;
	
	@Before
	public void before() {
		for (Titulo titulo : tituloBC.findAll()) {
			tituloBC.delete(titulo.getId_titulo());
		}
	}	
	
	
	@Test
	public void testInsert() {
				
		// modifique para inserir dados conforme o construtor
		Titulo titulo = new Titulo("descricao");
		tituloBC.insert(titulo);
		List<Titulo> listOfTitulo = tituloBC.findAll();
		assertNotNull(listOfTitulo);
		assertEquals(1, listOfTitulo.size());
	}	
	
	@Test
	public void testDelete() {
		
		// modifique para inserir dados conforme o construtor
		Titulo titulo = new Titulo("descricao");
		tituloBC.insert(titulo);
		
		List<Titulo> listOfTitulo = tituloBC.findAll();
		assertNotNull(listOfTitulo);
		assertEquals(1, listOfTitulo.size());
		
		tituloBC.delete(titulo.getId_titulo());
		listOfTitulo = tituloBC.findAll();
		assertEquals(0, listOfTitulo.size());
	}
	
	@Test
	public void testUpdate() {
		// modifique para inserir dados conforme o construtor
		Titulo titulo = new Titulo("descricao");
		tituloBC.insert(titulo);
		
		List<Titulo> listOfTitulo = tituloBC.findAll();
		Titulo titulo2 = (Titulo)listOfTitulo.get(0);
		assertNotNull(listOfTitulo);

		// alterar para tratar uma propriedade existente na Entidade Titulo
		// titulo2.setUmaPropriedade("novo valor");
		tituloBC.update(titulo2);
		
		listOfTitulo = tituloBC.findAll();
		Titulo titulo3 = (Titulo)listOfTitulo.get(0);
		
		// alterar para tratar uma propriedade existente na Entidade Titulo
		// assertEquals("novo valor", titulo3.getUmaPropriedade());
	}

}