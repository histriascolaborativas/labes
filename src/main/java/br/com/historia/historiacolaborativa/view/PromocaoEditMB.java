
package br.com.historia.historiacolaborativa.view;

import javax.inject.Inject;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractEditPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.com.historia.historiacolaborativa.business.*;
import br.com.historia.historiacolaborativa.domain.*;
import javax.faces.model.*;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import java.util.*;

// To remove unused imports press: Ctrl+Shift+o

@ViewController
@PreviousView("./promocao_list.jsf")
public class PromocaoEditMB extends AbstractEditPageBean<Promocao, Long> {

	private static final long serialVersionUID = 1L;

	@Inject
	private PromocaoBC promocaoBC;
	

	
	@Override
	@Transactional
	public String delete() {
		this.promocaoBC.delete(getId());
		return getPreviousView();
	}
	
	@Override
	@Transactional
	public String insert() {
		this.promocaoBC.insert(this.getBean());
		return getPreviousView();
	}
	
	@Override
	@Transactional
	public String update() {
		this.promocaoBC.update(this.getBean());
		return getPreviousView();
	}
	
	@Override
	protected Promocao handleLoad(Long id) {
		return this.promocaoBC.load(id);
	}	
}