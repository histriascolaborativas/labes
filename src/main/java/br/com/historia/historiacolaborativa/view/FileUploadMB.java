package br.com.historia.historiacolaborativa.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.RequestScoped; 
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
@ManagedBean(name="fileUploadMB") 
@RequestScoped 
public class FileUploadMB {
	public FileUploadMB() {
		
	} 
	
	public void doUpload(FileUploadEvent fileUploadEvent) { 
		UploadedFile uploadedFile = fileUploadEvent.getFile(); 
		String fileNameUploaded = uploadedFile.getFileName(); 
		long fileSizeUploaded = uploadedFile.getSize(); 
		String infoAboutFile = "<br/> Arquivo recebido: <b>" +fileNameUploaded +"</b><br/>"+ "Tamanho do Arquivo: <b>"+fileSizeUploaded+"</b>"; 
		FacesContext facesContext = FacesContext.getCurrentInstance(); 
		facesContext.addMessage(null, new FacesMessage("Sucesso", infoAboutFile)); 
		} 
	
	
	
	public void fileUploadAction(FileUploadEvent event) { 
		try { 
			FacesContext facesContext = FacesContext.getCurrentInstance(); 
	    	facesContext.addMessage(null, new FacesMessage("Sucesso", "PASSOU")); 		
	    	//ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext(); 
		//HttpServletResponse response = (HttpServletResponse) externalContext.getResponse(); 

		FacesContext aFacesContext = FacesContext.getCurrentInstance(); 
		ServletContext context = (ServletContext) aFacesContext.getExternalContext().getContext(); 

		String realPath = context.getRealPath("/"); 

		// Aqui cria o diretorio cash não exista 
		File file = new File("./fotos/"); 
		file.mkdirs(); 

		byte[] arquivo = event.getFile().getContents(); 
		String caminho = "/images/" + event.getFile().getFileName(); 

		// esse trecho grava o arquivo no diretório 
		FileOutputStream fos = new FileOutputStream(caminho); 
		fos.write(arquivo); 
		fos.close(); 


		} catch (Exception ex) { 
		System.out.println("Erro no upload de imagem" + ex); 
		} 
	}
	
	public void teste(FileUploadEvent event) throws IOException{
		UploadedFile file = event.getFile();
		byte[] arquivo = file.getContents();
		String nome = file.getFileName();
		String caminho = "./imagens/" + nome;
		FileOutputStream fos = new FileOutputStream(caminho); 
		fos.write(arquivo);
		fos.close();
		
	}
	
	public void carregarArquivo(FileUploadEvent event) // metodo chamado quando o arquivo acaba de carregar no serverSide  
            throws FileNotFoundException, IOException {  
          System.out.println("TESTE");
        FacesMessage msg = new FacesMessage("Sucesso "+event.getFile().getFileName()+" foi carregado.", event.getFile()  
                .getFileName() + " foi carregado."); // mensagem pra saber se ouve sucesso  
          
          
       String arquivo = event.getFile().getFileName(); // pego o nome do arquivo  
          
          
          
          
          
        String caminho = FacesContext.getCurrentInstance().getExternalContext()  
                .getRealPath("./imagens/"+arquivo ); // diretorio o qual vou salvar o arquivo do upload, equivale ao nome completamente qualificado  
          
          
  
  
        byte[] conteudo = event.getFile().getContents();  // daqui pra baixo é somente operações de IO.  
        FileOutputStream fos = new FileOutputStream(caminho);  
        fos.write(conteudo);  
        fos.close();  
          
          
          
          
  
    }

	}

