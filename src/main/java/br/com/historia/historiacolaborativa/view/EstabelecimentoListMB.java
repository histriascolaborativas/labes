package br.com.historia.historiacolaborativa.view;

import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import br.gov.frameworkdemoiselle.annotation.NextView;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractListPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.com.historia.historiacolaborativa.business.EstabelecimentoBC;
import br.com.historia.historiacolaborativa.domain.Estabelecimento;

@ViewController
@NextView("./estabelecimento_edit.jsf")
@PreviousView("./estabelecimento_list.jsf")
public class EstabelecimentoListMB extends AbstractListPageBean<Estabelecimento, Integer> {

	private static final long serialVersionUID = 1L;

	@Inject
	private EstabelecimentoBC estabelecimentoBC;
	
	@Override
	protected List<Estabelecimento> handleResultList() {
		return this.estabelecimentoBC.findAll();
	}
	
	@Transactional
	public String deleteSelection() {
		boolean delete;
		for (Iterator<Integer> iter = getSelection().keySet().iterator(); iter.hasNext();) {
			Integer id = iter.next();
			delete = getSelection().get(id);
			if (delete) {
				estabelecimentoBC.delete(id);
				iter.remove();
			}
		}
		return getPreviousView();
	}

}