
package br.com.historia.historiacolaborativa.view;

import javax.inject.Inject;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractEditPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.com.historia.historiacolaborativa.business.*;
import br.com.historia.historiacolaborativa.domain.*;
import javax.faces.model.*;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import java.util.*;

// To remove unused imports press: Ctrl+Shift+o

@ViewController
@PreviousView("./estabelecimento_list.jsf")
public class EstabelecimentoEditMB extends AbstractEditPageBean<Estabelecimento, Integer> {

	private static final long serialVersionUID = 1L;

	@Inject
	private EstabelecimentoBC estabelecimentoBC;
	

	
	@Override
	@Transactional
	public String delete() {
		this.estabelecimentoBC.delete(getId());
		return getPreviousView();
	}
	
	@Override
	@Transactional
	public String insert() {
		this.estabelecimentoBC.insert(this.getBean());
		return getPreviousView();
	}
	
	@Override
	@Transactional
	public String update() {
		this.estabelecimentoBC.update(this.getBean());
		return getPreviousView();
	}
	
	@Override
	protected Estabelecimento handleLoad(Integer id) {
		return this.estabelecimentoBC.load(id);
	}	
}