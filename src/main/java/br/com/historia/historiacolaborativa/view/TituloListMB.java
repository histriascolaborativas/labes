package br.com.historia.historiacolaborativa.view;

import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import br.gov.frameworkdemoiselle.annotation.NextView;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractListPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.com.historia.historiacolaborativa.business.TituloBC;
import br.com.historia.historiacolaborativa.domain.Titulo;

@ViewController
@NextView("./titulo_edit.jsf")
@PreviousView("./titulo_list.jsf")
public class TituloListMB extends AbstractListPageBean<Titulo, Long> {

	private static final long serialVersionUID = 1L;

	@Inject
	private TituloBC tituloBC;
	
	@Override
	protected List<Titulo> handleResultList() {
		return this.tituloBC.findAll();
	}
	
	@Transactional
	public String deleteSelection() {
		boolean delete;
		for (Iterator<Long> iter = getSelection().keySet().iterator(); iter.hasNext();) {
			Long id = iter.next();
			delete = getSelection().get(id);
			if (delete) {
				tituloBC.delete(id);
				iter.remove();
			}
		}
		return getPreviousView();
	}

}