
package br.com.historia.historiacolaborativa.view;

import javax.inject.Inject;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractEditPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.com.historia.historiacolaborativa.business.*;
import br.com.historia.historiacolaborativa.domain.*;
import javax.faces.model.*;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import java.util.*;

// To remove unused imports press: Ctrl+Shift+o

@ViewController
@PreviousView("./titulo_list.jsf")
public class TituloEditMB extends AbstractEditPageBean<Titulo, Long> {

	private static final long serialVersionUID = 1L;

	@Inject
	private TituloBC tituloBC;
	

	
	@Override
	@Transactional
	public String delete() {
		this.tituloBC.delete(getId());
		return getPreviousView();
	}
	
	@Override
	@Transactional
	public String insert() {
		this.tituloBC.insert(this.getBean());
		return getPreviousView();
	}
	
	@Override
	@Transactional
	public String update() {
		this.tituloBC.update(this.getBean());
		return getPreviousView();
	}
	
	@Override
	protected Titulo handleLoad(Long id) {
		return this.tituloBC.load(id);
	}	
}