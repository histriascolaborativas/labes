package br.com.historia.historiacolaborativa.view;

import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import br.gov.frameworkdemoiselle.annotation.NextView;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractListPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.com.historia.historiacolaborativa.business.PromocaoBC;
import br.com.historia.historiacolaborativa.domain.Promocao;

@ViewController
@NextView("./promocao_edit.jsf")
@PreviousView("./promocao_list.jsf")
public class PromocaoListMB extends AbstractListPageBean<Promocao, Long> {

	private static final long serialVersionUID = 1L;

	@Inject
	private PromocaoBC promocaoBC;
	
	@Override
	protected List<Promocao> handleResultList() {
		return this.promocaoBC.findAll();
	}
	
	@Transactional
	public String deleteSelection() {
		boolean delete;
		for (Iterator<Long> iter = getSelection().keySet().iterator(); iter.hasNext();) {
			Long id = iter.next();
			delete = getSelection().get(id);
			if (delete) {
				promocaoBC.delete(id);
				iter.remove();
			}
		}
		return getPreviousView();
	}

}