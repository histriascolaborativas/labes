package br.com.historia.historiacolaborativa.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = -5101613170027811788L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_usuario;

	@Column
	private String nome;

	@Column
	private Long id_facebook;

	@Column
	private String Foto_Perfil;

	@Column
	private Long qtd_comentarios;

	@Column
	private Long qtd_fotos;
	
	@Temporal(TemporalType.DATE)
	@Column
	private Date dataCadastro;

	@Column
	private String titulo;
/*	@ManyToOne(optional = true)
	@JoinColumn(name = "id_titulo")
	private Titulo titulo = new Titulo();

/*	@OneToOne(optional = true, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "ID_ENDERECO")
//	@NotNull
//	@Valid
	private Endereco endereco = new Endereco();

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE })
	@JoinColumn(name = "ID_COLABORADOR")
	private List<Telefone> telefones = new ArrayList<Telefone>();*/

	public Usuario() {
	}

	public Usuario(Integer id, String nome,
			String Foto_Perfil, Long facebook, Date data,
			Long qtd_fotos, String titulo, Long comentarios) {
		super();
		this.id_usuario = id;
		this.dataCadastro = data;
		this.nome = nome;
		this.Foto_Perfil = Foto_Perfil;
		this.id_facebook = facebook;
		this.qtd_comentarios = comentarios;
		this.qtd_fotos = qtd_fotos;
		this.titulo = titulo;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_usuario == null) ? 0 : id_usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (this.id_usuario == null) {
			if (other.id_usuario != null)
				return false;
		} else if (!id_usuario.equals(other.id_usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id_usuario + ", nome=" + nome + ", Titulo="
				+ titulo + "]";
	}

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId_facebook() {
		return id_facebook;
	}

	public void setId_facebook(Long id_facebook) {
		this.id_facebook = id_facebook;
	}

	public String getFoto_Perfil() {
		return Foto_Perfil;
	}

	public void setFoto_Perfil(String foto_Perfil) {
		Foto_Perfil = foto_Perfil;
	}

	public Long getQtd_comentarios() {
		return qtd_comentarios;
	}

	public void setQtd_comentarios(Long qtd_comentarios) {
		this.qtd_comentarios = qtd_comentarios;
	}

	public Long getQtd_fotos() {
		return qtd_fotos;
	}

	public void setQtd_fotos(Long qtd_fotos) {
		this.qtd_fotos = qtd_fotos;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}