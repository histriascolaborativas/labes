package br.com.historia.historiacolaborativa.domain;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Titulo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/*
	 *  If you are using Glassfish then remove the strategy attribute
	 */
	@Id
	@GeneratedValue(strategy = SEQUENCE)
	private Long id_titulo;
	
	@Column
	private String descricao;
	
	
	
	public Titulo() {
		super();
	}
	
	public Titulo(String descricao) {
		this.descricao = descricao;
		
	}
	
	public Long getId_titulo() {
		return this.id_titulo;
	}
	
	public void setId_titulo(Long id) {
		this.id_titulo = id;
	}
	
	public String getDescription() {
		return this.descricao;
	}
	
	public void setDescription(String descricao) {
		this.descricao = descricao;
	}
	
}
