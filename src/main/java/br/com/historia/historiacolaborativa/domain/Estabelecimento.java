package br.com.historia.historiacolaborativa.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;



@Entity
public class Estabelecimento implements Serializable {

	private static final long serialVersionUID = -5101613170027811788L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_estabelecimento;

	@Column
	private String horario;

	@Column
	private String site;

	@Column
	private String facebook;

	
	@Column
	private String promocao;
	/*@OneToOne(optional = true, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "id_promocao")
	private Promocao promocao = new Promocao();

/*	@OneToOne(optional = true, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "ID_ENDERECO")
//	@NotNull
//	@Valid
	private Endereco endereco = new Endereco();

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE })
	@JoinColumn(name = "ID_COLABORADOR")
	private List<Telefone> telefones = new ArrayList<Telefone>();*/

	public Estabelecimento() {
	}

	public Estabelecimento(Integer id, String site,
			String facebook, String horario, String promocao) {
		super();
		this.id_estabelecimento = id;
		this.facebook = facebook;
		this.horario = horario;
		this.site = site;
		this.promocao = promocao;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((facebook == null) ? 0 : facebook.hashCode());
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		result = prime
				* result
				+ ((id_estabelecimento == null) ? 0 : id_estabelecimento
						.hashCode());
		result = prime * result
				+ ((promocao == null) ? 0 : promocao.hashCode());
		result = prime * result + ((site == null) ? 0 : site.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estabelecimento other = (Estabelecimento) obj;
		if (facebook == null) {
			if (other.facebook != null)
				return false;
		} else if (!facebook.equals(other.facebook))
			return false;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		if (id_estabelecimento == null) {
			if (other.id_estabelecimento != null)
				return false;
		} else if (!id_estabelecimento.equals(other.id_estabelecimento))
			return false;
		if (promocao == null) {
			if (other.promocao != null)
				return false;
		} else if (!promocao.equals(other.promocao))
			return false;
		if (site == null) {
			if (other.site != null)
				return false;
		} else if (!site.equals(other.site))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id_estabelecimento + ", horario=" + horario + ", site="
				+ site + "]";
	}

	public Integer getId_estabelecimento() {
		return id_estabelecimento;
	}

	public void setId_estabelecimento(Integer id_estabelecimento) {
		this.id_estabelecimento = id_estabelecimento;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getPromocao() {
		return promocao;
	}

	public void setPromocao(String promocao) {
		this.promocao = promocao;
	}

}