package br.com.historia.historiacolaborativa.domain;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Promocao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/*
	 *  If you are using Glassfish then remove the strategy attribute
	 */
	@Id
	@GeneratedValue(strategy = SEQUENCE)
	private Long id_promocao;
	
	@Column
	private String descricao;
	
	
	
	public Promocao() {
		super();
	}
	
	public Promocao(String descricao) {
		this.descricao = descricao;
		
	}
	
	public Long getId_promocao() {
		return this.id_promocao;
	}
	
	public void setId_promocao(Long id) {
		this.id_promocao = id;
	}
	
	public String getDescription() {
		return this.descricao;
	}
	
	public void setDescription(String descricao) {
		this.descricao = descricao;
	}
	
}
