package br.com.historia.historiacolaborativa.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;
import br.gov.frameworkdemoiselle.util.Beans;
import br.com.historia.historiacolaborativa.persistence.EstabelecimentoDAO;
import br.com.historia.historiacolaborativa.domain.Estabelecimento;

@FacesConverter(value= "ConversorEstabelecimento")
public class EstabelecimentoConverter implements Converter {

	private EstabelecimentoDAO estabelecimentoDAO = Beans.getReference(EstabelecimentoDAO.class);

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		try{
		Object ret = null;
			if (component instanceof PickList) {
				Object dualList = ((PickList) component).getValue();
				DualListModel<?> dl = (DualListModel<?>) dualList;
				for (Object o : dl.getSource()) {
					String id = String.valueOf(((Estabelecimento) o).getId_estabelecimento());
					if (value.equals(id)) {
						ret = o;
						break;
					}
				}
				if (ret == null)
					for (Object o : dl.getTarget()) {
						String id = String.valueOf(((Estabelecimento) o).getId_estabelecimento());
						if (value.equals(id)) {
							ret = o;
							break;
						}
					}
			} else {
				if (value.trim().equals("")) {
					ret = null;
				} else {
					Integer varId = Integer.valueOf(value);
					ret =  estabelecimentoDAO.load(varId);
				}
			}
			return ret;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		try{
			if (value == null || value.equals("")) {
				return "";
			} else {			        
				return String.valueOf(((Estabelecimento) value).getId_estabelecimento());
			}
		}catch (Exception e) {
			e.printStackTrace();
			return "";
		}		
	}
}
