var map;
var chicago = {lat: 41.85, lng: -87.65};
var localizacao;

function initialize() {
	
    var latlng = new google.maps.LatLng(-1.457163, -48.496638);
    var options = {
        zoom: 15,
        center: {lat: -1.457163, lng: -48.496638},
        //disableDefaultUI: true,
        streetViewControl: false,
        zoomControl: true,
        zoomControlOptions: {
            //style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.TOP_RIGHT
            
          },
       // mapTypeId: google.maps.MapTypeId.ROADMAP
        
        
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.TOP_LEFT,
          mapTypeIds: [
            google.maps.MapTypeId.ROADMAP,
            google.maps.MapTypeId.TERRAIN,
            google.maps.MapTypeId.SATELLITE,
            google.maps.MapTypeId.HYBRID,
            google.maps.MapTypeId.TERRAIN
          ]
        }
        
     

        
    };
 
    map = new google.maps.Map(document.getElementById("map"), options);
    
    // Create the DIV to hold the control and call the CenterControl() constructor
    // passing in this DIV.
    var centerControlDiv = document.createElement('div');
    var centerControl = new CenterControl(centerControlDiv, map);

    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(centerControlDiv);
    ///////JSON
    var script = document.createElement('script');
    script.src = 'marcadores.js';
    document.getElementsByTagName('head')[0].appendChild(script);
      
    ///Marcador Teste
    
    var cinema = new google.maps.Marker({
        position: {lat:  -1.453635, lng: -48.493762},
      
        map: map,
        title: 'Click to zoom',
        icon: './estabelecimento/cinema/logoCine.png'
      }); 
    
    var pointacai = new google.maps.Marker({
        position: {lat: -1.448776, lng: -48.498440},
         
        map: map,
        title: 'Click to zoom',
        icon: './estabelecimento/pointacai/logo.jpg'
      });
    
    cinema.addListener('click', function() {
    	//infoWindow = new google.maps.InfoWindow();
    	//infoWindow.setContent("TEXTO TESTE");
    	//infoWindow.setPosition(cinema.getPosition());
    	//infoWindow.open(map);
        //map.setZoom(8);
       // map.setCenter(marker.getPosition());
    	window.location.replace("./estabelecimento/cinema/indexcinema.jsf");
      });
    
    pointacai.addListener('click', function() {
    	//infoWindow = new google.maps.InfoWindow();
    	//infoWindow.setContent("TEXTO TESTE");
    	//infoWindow.setPosition(cinema.getPosition());
    	//infoWindow.open(map);
        //map.setZoom(8);
       // map.setCenter(marker.getPosition());
    	window.location.replace("./estabelecimento/pointacai/indexpoint.jsf");
      });
    
    //////// Escuta para adicionar Foto
    map.addListener('click', function(e) {
    	var r = window.confirm('Você quer adicionar uma novo foto?');
    	if (r == true) {
    		placeMarkerAndPanTo(e.latLng, map);
        } else {
        	window.location.replace("index.jsf");
        }
        //document.getElementById("demo").innerHTML = txt;
        
      });
    
    
    
  //INICIAR PONTOS
	var arr_lines = [];
	//arr_lines = carregaLocalizacoes("file:///C:/Users/Heloisa/git/labes/src/main/webapp/mobi/base/base.txt");
	arr_lines = carregaLocalizacoes("./base/base.txt");
	//window.alert("Linha"+arr_lines[0]);
	var res = arr_lines[0].split(",");
	//window.alert("Resposta "+res[0]);
	/*var nome = 'CLick aqui!'; 
	
	var coord = {lat: -1.380445, lng: -48.444070}
	var marker = new google.maps.Marker({
	      map: map,
	      position: coord,
	      title: nome,
	   });*/
	//createMarker('1', '1',{lat: -1.380445, lng: -48.444070}, 'titulo', 'descricao', './estabelecimento/pointacai/logo.jpg', '1990');
	//createMarker({lat: -1.380445, lng: -48.444070}, 'titulo');
    displayMarkers();
}

///Função Que adiciona Marcador ao clicar
function placeMarkerAndPanTo(latLng, map) {
	  localizacao = latLng;
	  window.location.replace("adicionar.jsf");
	  var marker = new google.maps.Marker({
	    position: latLng,
	    map: map
	  });
	  map.panTo(latLng);
}

function eqfeed_callback(results) {
    map.data.addGeoJson(results);
  }

  // Call the initialize function after the page has finished loading
  google.maps.event.addDomListener(window, 'load', initialize);


 
function detectBrowser() {
	  var useragent = navigator.userAgent;
	  var mapdiv = document.getElementById("map");

	  if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1 ) {
	    mapdiv.style.width = '100%';
	    mapdiv.style.height = '100%';
	  } else {
	    mapdiv.style.width = '360px';
	    mapdiv.style.height = '640px';
	  }
}

function CenterControl(controlDiv, map) {

	  // Set CSS for the control border.
	  var controlUI = document.createElement('div');
	  controlUI.style.backgroundColor = '#fff';
	  controlUI.style.border = '2px solid #fff';
	  controlUI.style.borderRadius = '3px';
	  controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
	  controlUI.style.cursor = 'pointer';
	  controlUI.style.marginBottom = '22px';
	  controlUI.style.textAlign = 'center';
	  controlUI.title = 'Click to recenter the map';
	  controlDiv.appendChild(controlUI);

	  // Set CSS for the control interior.
	  var controlText = document.createElement('div');
	  controlText.style.color = 'rgb(25,25,25)';
	  controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
	  controlText.style.fontSize = '16px';
	  controlText.style.lineHeight = '38px';
	  controlText.style.paddingLeft = '5px';
	  controlText.style.paddingRight = '5px';
	  controlText.innerHTML = 'Filtrar';
	  controlUI.appendChild(controlText);

	  // Setup the click event listeners: simply set the map to Chicago.
	  controlUI.addEventListener('click', function() {
	    map.setCenter(chicago);
	  });

	}


detectBrowser();

initialize();
botaoTempo();

///// MÉTODOS LEITURA E ESCRITA DE ARQUIVO
function carregaLocalizacoes(pathArquivo){
	//file: O URL para o qual enviar a solicitação
	// ex: "/var/www/test/arquivo.txt" (alguns indicam usar 
	// "file:///C:/your/path/to/file.txt"
	
	var arr_lines = [];
	var rawFile = new XMLHttpRequest();
    rawFile.open("GET", pathArquivo, false);
    
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
            	locations = rawFile.responseText;
            	arr_lines = locations.split('\n');
                //alert(arr_lines[0]);	// Só pra debugar
                
                // Preenche os atributos de array de objetos
                // arr_obj_locations = locationsToObject(locations);	// Convert to array of objects Locations
            }else{
                aler(rawFile.readyState);
            }
        }
       
        else 
                    {
                        aler(rawFile.status);
                    }
    }
    rawFile.send(null);
    
    return arr_lines;
}


function addLocation(location){
	var str = "";
	for (var key in obj) {
	    if (str != "") {
	        str += "&";
	    }
	    str += key + "=" + encodeURIComponent(obj[key]);
	}
	
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange=function()
	{
	      if (xmlhttp.readyState==4 && xmlhttp.status==200)
	      {
	        alert(xmlhttp.responseText);
	      }
	}
	
	var params = encodeURIComponent(str); // "id=1&long=0&lat=0&img=url"
	xmlhttp.open("POST", "<<<DESTINO_DO_POST>>>", true);
	xmlhttp.send(params);
}



function displayMarkers(){

	var arr_lines = [];
	//arr_lines = carregaLocalizacoes("file:///C:/Users/Heloisa/git/labes/src/main/webapp/mobi/base/base.txt");
	arr_lines = carregaLocalizacoes("./base/base.txt");
	//window.alert("Linha"+arr_lines[0]);
	var res = arr_lines[0].split("#");
	/////////////////////////////////////////////////////////////
	
	var bounds2 = new google.maps.LatLngBounds();
	for (var j = 0; j < arr_lines.length; j++){
        var teste = {lat: -1.380445, lng: -48.444070};
		var res = arr_lines[j].split("#");
		var idMarcador = res[0];
		var idUsuario = res[1];
		var latitude = res[2];
		var longitude = res[3]
		var titulo = res[4];
		var descricao = res[5];
		var linkImagem = res[6];
		var dataFoto = res[7];
		//'1'#'1'#{lat: -1.380445, lng: -48.444070}#'titulo 1'#'descricao 1'#'./estabelecimento/pointacai/logo.jpg'#'1990'
        //window.alert(longitude);
		//window.alert("0 "+res[0]+"1 "+res[1]+"2 "+res[2]+"3 "+res[3]+"4 "+res[4]+"5 "+res[5]+"6 "+res[6]);
	     //createMarker('1', '1',{lat: -1.380445, lng: -48.444070}, 'titulo', 'descricao', './estabelecimento/pointacai/logo.jpg', '1990');
	     createMarker(res[0], res[1],res[2], res[3], res[4], res[5],res[6],res[7]);
		//createMarker(idMarcador, idUsuario,coordenadas,titulo, descricao, linkImagem,dataFoto);
		//createMarker(latitude, longitude,'titulo');
		
	}
	
	
	//////////////////////////////////////////////////////////////////
	//window.alert("Resposta "+res[0]);
   
	/*
	var markersData
	   // esta variável vai definir a área de mapa a abranger e o nível do zoom
	   // de acordo com as posições dos marcadores
	   var bounds = new google.maps.LatLngBounds();

	   // Loop que vai percorrer a informação contida em markersData 
	   // para que a função createMarker possa criar os marcadores 
	   for (var i = 0; i < markersData.length; i++){

	      var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
	      var nome = markersData[i].nome;
	      var morada1 = markersData[i].morada1;
	      var morada2 = markersData[i].morada2;
	      var codPostal = markersData[i].codPostal;

	      createMarker(latlng, nome, morada1, morada2, codPostal);
	      createMarker('1', '1',{lat: -1.380445, lng: -48.444070}, 'titulo', 'descricao', './estabelecimento/pointacai/logo.jpg', '1990');

	      // Os valores de latitude e longitude do marcador são adicionados à
	      // variável bounds
	      bounds.extend(latlng); 
	   }

	   // Depois de criados todos os marcadores,
	   // a API, através da sua função fitBounds, vai redefinir o nível do zoom
	   // e consequentemente a área do mapa abrangida de acordo com
	   // as posições dos marcadores
	   map.fitBounds(bounds);*/
}


//Função que cria os marcadores e define o conteúdo de cada Info Window.
function createMarker(idMarcador, idUsuario,latitude,longitude, titulo, descricao, linkImagem, dataFoto){
//function createMarker(latitude, longitude, titulo){


   var marker = new google.maps.Marker({
      map: map,
      position: new google.maps.LatLng(latitude, longitude),
      title: titulo,
      icon: './imagens/photo.png'
   });
   //window.alert(marker.getAttribute(latitude));
   //marker.getAttribute(latitude);
   // Evento que dá instrução à API para estar alerta ao click no marcador.
   // Define o conteúdo e abre a Info Window.
   
   marker.addListener('click', function() {
	   document.getElementById("map").innerHTML = "TESTE";
	   jQuery("#map").html("<div style=\" text-align:center; background-color:#FFFFFF;\">" +
	   		"<h2>"+ titulo + "<h2 />" +
	   		"<img src=\"" + linkImagem + "\" width=300px >" + "<br>" +
	   		"" + descricao + "<br>" +
	   		"" + dataFoto + "<br>" +
	   		"<div />");
   	//window.location.replace("./estabelecimento/pointacai/indexpoint.jsf");
     });
  
   /*
   google.maps.event.addListener(marker, 'click', function() {
      
      // Variável que define a estrutura do HTML a inserir na Info Window.
      var iwContent = '<div id="iw_container">' +
      '<div class="iw_title">' + titulo + '</div>' +
      '<div class="iw_content">' + idUsuario + '<br />' +
      idMarcador + '<br />' +
      descricao + '<br />' + 
      linkImagem + '<br />' + 
      dataFoto + '<br />' + 
      '</div></div>';
      
      // O conteúdo da variável iwContent é inserido na Info Window.
      infoWindow.setContent(iwContent);
      
      // A Info Window é aberta com um click no marcador.
      infoWindow.open(map, marker);
   });*/
}
